from django.contrib import admin
from cmdb.models import Idc, HostList, HostInfo

admin.site.register(Idc)
admin.site.register(HostList)
admin.site.register(HostInfo)
