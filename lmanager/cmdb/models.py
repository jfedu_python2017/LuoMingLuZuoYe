from django.db import models
from django.contrib.auth.models import User

class  Idc(models.Model):
    """定义机房类"""
    owner = models.ForeignKey(User)
    idc_name = models.CharField(max_length=40, verbose_name=u'机房名称')
    remark = models.CharField(max_length=50, verbose_name=u'备注')
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name='机房'
        verbose_name_plural = '机房'

    def __str__(self):
        """结果返回idc_name"""
        return self.idc_name

class HostList(models.Model):
        """定义主机列表"""
        idc_name = models.ForeignKey(Idc)
        date_added = models.DateTimeField(auto_now=True)
        ip = models.GenericIPAddressField(protocol='both',unpack_ipv4=False, verbose_name="ip")
        hostname = models.CharField(max_length=40, verbose_name=u'主机名称')
        bianhao = models.CharField(max_length=20, verbose_name=u'编号')
        application = models.CharField(max_length=40, verbose_name=u'应用')

        class Meta:
            verbose_name = '主机列表'
            verbose_name_plural = '主机列表'

        def __str__(self):
            return self.ip

class HostInfo(models.Model):
    '''定义主机详细信息'''
    host_list = models.ForeignKey(HostList)
    manufacturer = models.CharField(max_length=40, verbose_name=u'厂商')
    productmode = models.CharField(max_length=40, verbose_name=u'产品型号')
    serialnumber = models.CharField(max_length=40, verbose_name=u'产品序列号')
    cpu = models.CharField(max_length=40, verbose_name=u'CPU核数')
    mem = models.CharField(max_length=40, verbose_name=u'内存')
    hostname = models.CharField(max_length=40, verbose_name=u'主机名')
    os = models.CharField(max_length=40, verbose_name=u'操作系统')
    disk = models.CharField(max_length=40, verbose_name=u'硬盘大小')
    ip = models.GenericIPAddressField(protocol='both',unpack_ipv4=False, verbose_name='ip')

    class Meta:
        verbose_name = '主机信息'
        verbose_name_plural = '主机信息'

    def __str__(self):
        return self.hostname
